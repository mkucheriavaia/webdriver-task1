package com.epam.rd.aqa.webdriver.task1;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class PastebinComTest {

    public static final String HTTPS_PASTEBIN_COM = "https://pastebin.com/";
    public static final By COOKIE_BANNER_AGREE_BTN = By.xpath("//button[@mode='primary']");
    public static final By CODE_TEXTAREA = By.id("postform-text");
    public static final By PASTE_EXPIRATION_DROPDOWN = By.id("select2-postform-expiration-container");
    public static final By TEN_MINUTES_PASTE_EXPIRATION = By.xpath("//li[text()='10 Minutes']");
    public static final By PASTE_NAME_INPUT_FIELD = By.id("postform-name");
    public static final By CREATE_NEW_PASTE_BTN = By.cssSelector("button.btn");

    @Test
    public void createPastebin() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        openPastebinComHomePage(driver);

        insertCode(driver, "Hello from WebDriver");

        setPasteExpirationToTenMinutes(driver);

        setPasteName(driver, "helloweb");

        clickOnCreateNewPaste(driver);

        driver.quit();
    }

    private void openPastebinComHomePage(WebDriver driver) {
        driver.get(HTTPS_PASTEBIN_COM);
        Optional.ofNullable(
                new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS))
                        .until(ExpectedConditions.elementToBeClickable(COOKIE_BANNER_AGREE_BTN)))
                .ifPresent(WebElement::click);
    }

    private void insertCode(WebDriver driver, String code) {
        WebElement textArea = driver.findElement(CODE_TEXTAREA);
        textArea.sendKeys(code);
    }

    private void setPasteExpirationToTenMinutes(WebDriver driver) {
        WebElement pastExpiration = driver.findElement(PASTE_EXPIRATION_DROPDOWN);
        pastExpiration.click();
        driver.findElement(TEN_MINUTES_PASTE_EXPIRATION).click();
    }

    private void setPasteName(WebDriver driver, String name) {
        WebElement pasteName = driver.findElement(PASTE_NAME_INPUT_FIELD);
        pasteName.sendKeys(name);
    }

    private void clickOnCreateNewPaste(WebDriver driver) {
        WebElement createNewPasteBtn = driver.findElement(CREATE_NEW_PASTE_BTN);
        createNewPasteBtn.click();
    }

}